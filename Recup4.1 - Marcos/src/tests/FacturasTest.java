package tests;

import static org.junit.Assert.*;

import org.junit.BeforeClass;

import org.junit.Test;

import clases.Factura;
import clases.GestorContabilidad;

public class FacturasTest {
	
	static Factura facturaPrueba;
	static GestorContabilidad gestor;
	
	@BeforeClass
	public static void prepararClasePruebas(){
		facturaPrueba = new Factura();
		gestor = new GestorContabilidad();
	}
	
	
	@Test
	public void testCalcularPrecioProducto1(){
		facturaPrueba.setCantidad(3);
		facturaPrueba.setPrecioUnidad(2.5F);
		
		float esperado = 7.5F;
		float actual = facturaPrueba.calcularPrecioTotal();
		
		assertEquals(esperado, actual, 0.0);	
	}
	
	@Test
	public void testCalcularPrecioCantidadCero(){
		facturaPrueba.setCantidad(0);
		facturaPrueba.setPrecioUnidad(2.5F);
		
		float esperado = 0.0F;
		float actual = facturaPrueba.calcularPrecioTotal();
		
		assertEquals(esperado, actual, 0.0);
	}
	
	@Test
	public void testCalcularPrecioConPrecioNegativo(){
		facturaPrueba.setCantidad(2);
		facturaPrueba.setPrecioUnidad(-1.5F);
		
		float esperado = -3.0F;
		float actual = facturaPrueba.calcularPrecioTotal();
		
		assertEquals(esperado, actual, 0.0);
	}
	
	
	@Test 
	public void comprobarFacturaInexistente(){
		GestorContabilidad gestor = new GestorContabilidad();
		Factura esperada = gestor.buscarFactura("12345");
		assertNull(esperada);
	}
	
	@Test 
	public void comprobarFacturaExistente(){
		
		Factura esperada = new Factura();
		esperada.setCodigoFactura("1234");
		gestor.getListaFacturas().add(esperada);
		
		Factura actual = gestor.buscarFactura("1234");
		assertSame(esperada,actual);
	}
	//calcular la factura mas cara
			@Test
			void testCalcularFacturaMasCara() {
				facturaPrueba.setCantidad(5);
				facturaPrueba.setPrecioUnidad(2);
				
				double esperado = 10;
				
				facturaPrueba.setCantidad(1);
				facturaPrueba.setPrecioUnidad(2);
				double esperado2 = 2;
				
				double actual= 10;
				
				assertEquals(esperado, actual,0);
			}
			//calcular la factura mas cara con precio negativo
			@Test
			void testCalcularFacturaMasCaraConPreciosNegativos() {
				facturaPrueba.setCantidad(2);
				facturaPrueba.setPrecioUnidad(-2);
				
				double esperado = -4;
				
				facturaPrueba.setCantidad(2);
				facturaPrueba.setPrecioUnidad(-3);
				
				double actual = -6;
				
				assertEquals(esperado, actual,0);
			}
			//calcular la facturacion en un a�o
			@Test 
			void testCalcularFacturacionAnual() {
				Factura unafactura= new Factura(2, 1,1991);
				Factura unafactura1= new Factura(2, 2, 1992);
				Factura unafactura2= new Factura(2, 3, 2000);
				
				gestor.getListaFacturas().add(unafactura);
				gestor.getListaFacturas().add(unafactura1);
				gestor.getListaFacturas().add(unafactura2);
				double resultado;
				resultado=6;
				double actual=gestor.calcularFacturacionAnual(2000);
				assertEquals(actual, resultado, 0);
			}
			//calcular la facturacion anual en un a�o que sea nulo
			@Test
			void testCalcularFacturacionAnualNula() {
				Factura unafactura= new Factura(1,0 ,1999);
				gestor.getListaFacturas().add(unafactura);
				int actual=gestor.calcularFacturacionAnual(1990);
				int esperado=0;
				assertEquals(actual, esperado, 0);
			}
			//calcular la cantidad de facturas que tiene un cliente
			@Test
			void testCantidadFacturasPorCliente(String dni) {
				int esperado=2;
				int resultado=gestor.cantidadFacturasPorCliente("pepito");
				
				assertEquals(esperado, resultado);
			}
			//eliminar una factura inexistente
			@Test
			public void eliminarFacturaInexistente(){
				GestorContabilidad gestorPruebas = new GestorContabilidad();
				gestorPruebas.eliminarFactura("789");
				fail("Se esperaba nullponterException");
			}
			//eliminar una factura existente
			@Test
			public void eliminarFacturaExistente(){
				GestorContabilidad gestorPruebas = new GestorContabilidad();
				gestorPruebas.getListaFacturas().clear();
				gestorPruebas.getListaFacturas().add(facturaPrueba);
				gestorPruebas.eliminarFactura("4");
				boolean x = gestorPruebas.getListaFacturas().contains(facturaPrueba);
				assertFalse(x);
			}
	
}
