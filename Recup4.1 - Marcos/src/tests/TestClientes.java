package tests;

import static org.junit.Assert.*;

import java.time.LocalDate;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestClientes {

	static	GestorContabilidad gestorPruebas;
	Cliente actual;
	
	@BeforeClass
	public static void inicializarClase(){
		gestorPruebas = new GestorContabilidad();
	}

	@Test
	public void testBuscarClienteInexistenteSinClientes() {
		//Busco un dni que no exista, debe devolver null
		String dni = "2345234";
		
		Cliente actual = gestorPruebas.buscarCliente(dni);	
		assertNull(actual);
	}

	@Test
	public void testBuscarClienteExistente(){
		//busca un cliente que si exista, el resultado debe ser el mismo objeto
		Cliente esperado = new Cliente("1234F", "Fernando", LocalDate.now());
		//A�ado un cliente a la lista de clientes
		gestorPruebas.getListaClientes().add(esperado);

		actual = gestorPruebas.buscarCliente("1234F");

		assertSame(esperado, actual);
	}
	
	@Test
	public void testBuscarClienteInexistenteConClientes(){	
		String dni = "64F";
		//Busco un objeto que no exista, el resultado debe ser null
		actual = gestorPruebas.buscarCliente(dni);	
		assertNull(actual);
	}

	@Test
	public void testBuscarClienteHabiendoVariosClientes(){
		Cliente esperado = new Cliente("34567F", "Maria", LocalDate.parse("1990-05-04"));
		gestorPruebas.getListaClientes().add(esperado);
		String dniABuscar = "1234678L";
		esperado = new Cliente(dniABuscar, "Pedro", LocalDate.parse("1995-07-02"));
		gestorPruebas.getListaClientes().add(esperado);
		
		actual = gestorPruebas.buscarCliente(dniABuscar);
		
		assertSame(esperado, actual);
	}
	//dar de alta por primera vez a un cliente
		@Test
		public void testAltaPrimerCliente() {
			gestorPruebas.getListaClientes().clear();
			Cliente nuevoCliente=new Cliente("1234G","Pablo",LocalDate.now());
			gestorPruebas.altaCliente(nuevoCliente);
			
			boolean actual = gestorPruebas.getListaClientes().contains(nuevoCliente);
			
			assertTrue(actual);
		}
		//dar de alta a varios clientes
		@Test
		public void testAltaVariosClientes() {
			Cliente cliente=new Cliente("262l","Diego",LocalDate.parse("2017-05-01"));
			gestorPruebas.altaCliente(cliente);
			cliente=new Cliente("223l","Diego",LocalDate.parse("2016-04-02"));
			gestorPruebas.altaCliente(cliente);
			boolean actual = gestorPruebas.getListaClientes().contains(cliente);
			
			assertTrue(actual);
		}
		//calcular cuando ya hay un cliente con el mismo dni
		@Test
		public void testClienteConMismoDni() {
			Cliente cliente = new Cliente("23RJ", "Pepe", LocalDate.now());
			gestorPruebas.altaCliente(cliente);
			Cliente cliente2 = new Cliente ("23RJ", "Juan", LocalDate.now());
			gestorPruebas.altaCliente(cliente2);
			
			boolean esperado = gestorPruebas.getListaClientes().contains(cliente2);
			
			assertFalse(esperado);
		}
		//calcular el cliente mas antiguo
		@Test
		public void testClienteMasAntiguo() {
			Cliente cliente = new Cliente("1233", "JoseLuis", LocalDate.parse("1980-01-01"));
			gestorPruebas.getListaClientes().add(cliente);
			Cliente nuevoCliente = new Cliente("53123", "Cristian", LocalDate.parse("2000-05-01"));
			gestorPruebas.getListaClientes().add(nuevoCliente);
			Cliente esperado = gestorPruebas.clienteMasAntiguo();
			assertEquals(cliente,esperado);
		}
		
		//calcular el cliente mas antiguo con un cliente que no existe
		@Test
		public void testClienteMasAntiguoConClienteInexistente() {
			String nombre = "Daniel";
			
			actual = gestorPruebas.buscarCliente(nombre);
			assertNull(actual);
		}
		//asignar un cliente a una factura
		@Test
		void testAsignarClienteAFactura(String dni, String codigoFactura) {
			Cliente unCliente = new Cliente("1234h","juan", LocalDate.now());
			gestorPruebas.asignarClienteAFactura("1234h", "124");
		}
		//eliminar un cliente
		@Test
		void testEliminarCliente(String dni) {
			Cliente unCliente = new Cliente("sdfsdf", "ramos", LocalDate.now());
			gestorPruebas.getListaClientes().add(unCliente);
			
			gestorPruebas.eliminarCliente("sdfsdf");
			boolean error=gestorPruebas.getListaClientes().contains(unCliente);
			assertFalse(error);
		}
		//eliminar cliente con factura asignada
		@Test
		void testEliminarClienteConFactura() {
			Cliente cliente = new Cliente("14213", "Maria",LocalDate.now());
			gestorPruebas.getListaClientes().add(cliente);
			Factura factura = new Factura(20,3,2018);
			gestorPruebas.getListaFacturas().add(factura);
			
			gestorPruebas.eliminarCliente("14213");
			
			boolean esperado = gestorPruebas.getListaClientes().contains(cliente);
			
			assertFalse(esperado);
			
		}
		//eliminar cliente inexistente
		@Test
		void testEliminarClienteInexistente() {
			Cliente cliente = new Cliente("4142f", "sacro", LocalDate.now());
			gestorPruebas.eliminarCliente("4");
			boolean esperado = gestorPruebas.getListaClientes().contains(cliente);
			
			assertFalse(esperado);
		}
}
