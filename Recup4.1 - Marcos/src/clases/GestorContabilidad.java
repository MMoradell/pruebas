package clases;

import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Factura> listaFacturas;
	
	public GestorContabilidad(){
		listaClientes = new ArrayList<>();
		listaFacturas = new ArrayList<>(); 
	}

	//Metodo Temporal para facilitar las pruebas
	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

	//Metodo Temporal para facilitar las pruebas
	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}
	
	
	public Cliente buscarCliente(String dni){
		for (Cliente cliente : listaClientes) {
			if(cliente.getDni().equals(dni)){
				return cliente;
			}
		}
		return null;
	}
	
	public Factura buscarFactura(String codigo){
		if(listaFacturas.isEmpty()){
			return null;
		}
		return listaFacturas.get(0);
	}
	public  Cliente clienteMasAntiguo() {
		return null;
		
	}
	public void eliminarFactura(String dni) {
		// TODO Auto-generated method stub
		
	}

	public int calcularFacturacionAnual(int anno) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int cantidadFacturasPorCliente(String dni) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void asignarClienteAFactura(String dni, String codigoFactura) {
		// TODO Auto-generated method stub
		
	}

	public void eliminarCliente(String dni) {
		// TODO Auto-generated method stub
		
	}

	public void altaCliente(Cliente cliente) {
		// TODO Auto-generated method stub
		
	}
}
