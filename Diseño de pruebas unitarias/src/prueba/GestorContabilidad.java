package prueba;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class GestorContabilidad {
	Scanner in = new Scanner(System.in);
	ArrayList<Factura> listaFacturas ;
	ArrayList<Cliente> listaClientes ;
	
	public GestorContabilidad() {
		listaFacturas = new ArrayList<Factura>();
		listaClientes = new ArrayList<Cliente>();
	}
	
	
	public Cliente buscarCliente(String dni) {
		for(int i=0; i<listaClientes.size(); i++) {
			if(dni.equalsIgnoreCase(listaClientes.get(i).getDni())) {
				return listaClientes.get(i);
			}else {
				return null;
			}
		}
		return null;	
	}
	public Factura buscarFactura(String codigo) {
		for(int i=0; i<listaFacturas.size(); i++) {
			if(codigo.equalsIgnoreCase(listaFacturas.get(i).getCodigoFactura())) {
				return listaFacturas.get(i);
			}else {
				return null;
			}
		}
		return null;
	}
	public void altaCliente(Cliente cliente) {
		for(int i=0; i<listaClientes.size(); i++) {
			if(cliente.equals(listaClientes.get(i).getDni())){
				
			}else {
				listaClientes.add(cliente);
			}
		}
	}
	public void crearFactura(Factura factura) {
		boolean repetido=false;
		for(int i=0; i<listaFacturas.size(); i++) {
			if(listaFacturas.get(i).getCodigoFactura()==factura.getCodigoFactura()){
				repetido=true;
			}else {
				repetido=false;
			}
		}
	}
	Cliente clienteMasAntiguo() {
		for(int i=0; i<listaClientes.size(); i++) {
			if(listaClientes.get(i).getDni()==null) {
				return null;
			}else {
				
			}
		}
		return null;
		
	}

	//Metodo temporal para facilitar las pruebas
	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}


	
}
