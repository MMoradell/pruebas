package tests;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import prueba.Cliente;
import prueba.Factura;
import prueba.GestorContabilidad;

public class FacturasTest {
	
	@Test
	public void testCalcularPrecioProducto1() {
		Factura facturaPrueba = new Factura();
		facturaPrueba.setCantidad(3);
		facturaPrueba.setPrecioUnidad(2.5F);
		float esperado = 7.5F;
		float actual = facturaPrueba.calcularPrecioTotal();
		
		assertEquals(esperado, actual, 0.0);
	}
	@Test
	public void testCalcularPrecioConPrecioNegativo() {
		Factura facturaPrueba = new Factura();
		facturaPrueba.setCantidad(3);
		facturaPrueba.setPrecioUnidad(-2.5F);
		float esperado = -7.5F;
		float actual = facturaPrueba.calcularPrecioTotal();
		
		assertEquals(esperado, actual, 0.0);
	}
	@Test
	public void testCalcularPrecioCantidadCero() {
		Factura facturaPrueba = new Factura();
		facturaPrueba.setCantidad(0);
		facturaPrueba.setPrecioUnidad(2.5F);
		float esperado = 0F;
		float actual = facturaPrueba.calcularPrecioTotal();
		
		assertEquals(esperado, actual, 0.0);
	}
	
}
