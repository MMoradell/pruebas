package tests;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import prueba.Cliente;
import prueba.GestorContabilidad;

public class ClientesTest {
	static GestorContabilidad gestor = new GestorContabilidad();
	@Test
	public void testComprobarCliente() {
		String dni="1234V";
		String actual = dni;
		gestor.buscarCliente("1234V");
		assert (actual) != null;
	}
	@Test
	public void testComprobarClienteInexistente() {
		Cliente clientePrueba = new Cliente();
		clientePrueba.setDni("123498V");
		if(gestor.buscarCliente("999H") == null) {
			
		}
	}
	@Test
	public void testComprobarAlta() {
		Cliente clientePrueba = new Cliente("Pepe", "121212K", LocalDate.now());
		if(gestor.altaCliente("Pepe", "121212K", LocalDate.now())){
			
		}
	}
	@Test
	public void testComprobarAltaConClienteMismoDni() {
		Cliente clientePrueba = new Cliente();
		
	}

}
