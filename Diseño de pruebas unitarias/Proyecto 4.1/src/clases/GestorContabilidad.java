package clases;

import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;
	
	public GestorContabilidad() {
		super();
		listaFacturas = new ArrayList<>();
		listaClientes = new ArrayList<>();
	}


	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}


		//Metodo Temporal para facilitar las pruebas
	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}


	
	public Cliente buscarCliente(String dni) {
		
	
		return null;
	}

	
	public Factura buscarFactura(String codigo) {
		
		return null;
		
	}
	
	public void altaCliente(Cliente cliente) {
	
	}
	
	public void crearFactura(Factura factura) {
		
	}
	
	public Cliente clienteMasAntiguo() {
		
		return null;
		
	}
	
	public Factura facturaMasCara() {
		
		return null;
		
	}
	
	public String calcularFacturacionAnual(String anno) {
		
		return anno;
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		
		
	}
	
	public int cantidadFacturasPorCliente(String dni) {
		
		return 0;
		
	}
	
	public void eliminarFactura(String c�digo) {
		
		
	}
	
	public void eliminarCliente(String dni) {
		
	}


	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}


	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}
	
	
}
