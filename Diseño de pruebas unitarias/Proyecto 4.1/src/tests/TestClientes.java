package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestClientes {
	static	GestorContabilidad gestorPruebas;
	Cliente actual;
	//iniciarlizar el objeto
	@BeforeClass
	public static void inicializarClase(){
		gestorPruebas = new GestorContabilidad();
	}
	//buscar un cliente con un dni que no exista
	@Test
	public void testBuscarClienteInexistenteSinClientes() {
		//Busco un dni que no exista, debe devolver null
		String dni = "2345234";
		
		Cliente actual = gestorPruebas.buscarCliente(dni);	
		assertNull(actual);
	}
	//buscar un cliente con dni existente
	@Test
	public void testBuscarClienteExistente(){
		//busca un cliente que si exista, el resultado debe ser el mismo objeto
		Cliente esperado = new Cliente("1234F", "Juan", LocalDate.now());
		//A�ado un cliente a la lista de clientes
		gestorPruebas.getListaClientes().add(esperado);

		actual = gestorPruebas.buscarCliente("1234F");

		assertSame(esperado, actual);
	}
	//buscar un cliente con un dni que no exista pero el cliente si
	@Test
	public void testBuscarClienteInexistenteConClientes(){	
		String dni = "64F";
		//Busco un objeto que no exista, el resultado debe ser null
		actual = gestorPruebas.buscarCliente(dni);	
		assertNull(actual);
	}
	//buscar un cliente con varios clientes existentes
	@Test
	public void testBuscarClienteHabiendoVariosClientes(){
		Cliente esperado = new Cliente("34567F", "Jose", LocalDate.parse("1990-05-04"));
		gestorPruebas.getListaClientes().add(esperado);
		String dniABuscar = "1234678L";
		esperado = new Cliente(dniABuscar, "Jose", LocalDate.parse("1995-07-02"));
		gestorPruebas.getListaClientes().add(esperado);
		
		actual = gestorPruebas.buscarCliente(dniABuscar);
		
		assertSame(esperado, actual);
	}
	//dar de alta por primera vez a un cliente
	@Test
	public void testAltaPrimerCliente() {
		gestorPruebas.getListaClientes().clear();
		Cliente nuevoCliente=new Cliente("1234G","Pablo",LocalDate.now());
		gestorPruebas.altaCliente(nuevoCliente);
		
		boolean actual = gestorPruebas.getListaClientes().contains(nuevoCliente);
		
		assertTrue(actual);
	}
	//dar de alta a varios clientes
	@Test
	public void testAltaVariosClientes() {
		Cliente nuevoCliente=new Cliente("262l","Diego",LocalDate.parse("2017-05-01"));
		gestorPruebas.altaCliente(nuevoCliente);
		nuevoCliente=new Cliente("223l","Diego",LocalDate.parse("2016-04-02"));
		gestorPruebas.altaCliente(nuevoCliente);
		boolean actual = gestorPruebas.getListaClientes().contains(nuevoCliente);
		
		assertTrue(actual);
	}
	//calcular el cliente mas antiguo
	@Test
	public void testClienteMasAntiguo() {
		Cliente esperado = new Cliente("1233", "JoseLuis", LocalDate.parse("1980-01-01"));
		gestorPruebas.getListaClientes().add(esperado);
		Cliente nuevoCliente = new Cliente("53123", "Cristian", LocalDate.parse("2000-05-01"));
		gestorPruebas.getListaClientes().add(nuevoCliente);
		boolean actual = gestorPruebas.getListaClientes().contains(esperado);
		
		assertTrue(actual);
	}
	//calcular el cliente mas antiguo que no exista
	@Test
	public void testClienteMasAntiguoInexistente() {
		String nombre = "Daniel";
		
		actual = gestorPruebas.buscarCliente(nombre);
		assertNull(actual);
	}
	//asignar un cliente a una factura
	@Test
	void testAsignarClienteAFactura(String dni, String codigoFactura) {
		Cliente unCliente = new Cliente("1234h","juan", LocalDate.now());
		gestorPruebas.asignarClienteAFactura("1234h", "124");
	}
	//eliminar un cliente
	@Test
	void testEliminarCliente(String dni) {
		Cliente unCliente = new Cliente("sdfsdf", "ramos", LocalDate.now());
		gestorPruebas.getListaClientes().add(unCliente);
		
		gestorPruebas.eliminarCliente("sdfsdf");
		boolean error=gestorPruebas.getListaClientes().contains(unCliente);
		assertFalse(error);
	}
}
