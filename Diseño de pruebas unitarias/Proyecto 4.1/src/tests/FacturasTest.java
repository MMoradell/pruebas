	package tests;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

import clases.Factura;
import clases.GestorContabilidad;

import static java.lang.Math.*;
class FacturasTest {
	static Factura facturaPrueba;
	static GestorContabilidad gestorPrueba;
	
	//Para iniciarlizar los objetos
	@BeforeClass
	public static void prepararClasePruebas() {
		facturaPrueba = new Factura();
		gestorPrueba = new GestorContabilidad();
	}
	//Calcular un precio con una cantidad
		@Test
		
		 void testCalcuclarPrecioProducto1() {
			facturaPrueba.setCantidad(3);
			facturaPrueba.setPrecioUnidad(2.5);
			
			double esperado =7.5;
			double actual=facturaPrueba.calcularPrecioTotal();
			
			assertEquals(esperado,actual,0);
		}
		//calcular un precio con una cantidad a 0 
		@Test
		 void testCalcularCantidadCero() {
			facturaPrueba.setCantidad(0);
			facturaPrueba.setPrecioUnidad(2.5);
			
			double esperado =0;
			double actual=facturaPrueba.calcularPrecioTotal();
			
			assertEquals(esperado,actual,0);
			
		}
		//calcular un precio negativo con cantidad
		@Test
		void testCalcularPrecioConPrecioNegativo() {
			facturaPrueba.setCantidad(2);
			facturaPrueba.setPrecioUnidad(-1.5);
			
			double esperado =-3;
			double actual=facturaPrueba.calcularPrecioTotal();
			
			assertEquals(esperado,actual,0);
			
		}
		//calcular la factura mas cara
		@Test
		void testCalcularFacturaMasCara() {
			facturaPrueba.setCantidad(5);
			facturaPrueba.setPrecioUnidad(2);
			
			double esperado = 10;
			
			facturaPrueba.setCantidad(1);
			facturaPrueba.setPrecioUnidad(2);
			double esperado2 = 2;
			
			double actual= 10;
			
			assertEquals(esperado, actual,0);
		}
		//calcular la factura mas cara con precio negativo
		@Test
		void testCalcularFacturaMasCaraConPreciosNegativos() {
			facturaPrueba.setCantidad(2);
			facturaPrueba.setPrecioUnidad(-2);
			
			double esperado = -4;
			
			facturaPrueba.setCantidad(2);
			facturaPrueba.setPrecioUnidad(-3);
			
			double actual = -6;
			
			assertEquals(esperado, actual,0);
		}
		//calcular la facturacion en un a�o
		@Test 
		void testCalcularFacturacionAnual() {
			Factura unafactura= new Factura(2, 1,"1991");
			Factura unafactura1= new Factura(2, 5,"1991");
			Factura unafactura2= new Factura(2, (float) 2.5,"2000");
			
			gestorPrueba.getListaFacturas().add(unafactura);
			gestorPrueba.getListaFacturas().add(unafactura1);
			gestorPrueba.getListaFacturas().add(unafactura2);
			float resultado;
			resultado=gestorPrueba.calcularFacturacionAnual("1991");
			float esperado=12;
			assertEquals(esperado, resultado, 0);
		}
		//calcular la facturacion anual en un a�o que sea nulo
		@Test
		void testCalcularFacturacionAnualNula() {
			Factura unafactura= new Factura(1, "1999");
			gestorPrueba.getListaFacturas().add(unafactura);
			String facturaAnual=gestorPrueba.calcularFacturacionAnual("1990");
			assertEquals(facturaAnual, 0, 0);
		}
		//calcular la cantidad de facturas que tiene un cliente
		@Test
		void testCantidadFacturasPorCliente(String dni) {
			int esperado=2;
			int resultado=gestorPrueba.cantidadFacturasPorCliente("pepito");
			
			assertEquals(esperado, resultado);
		}
		//eliminar una factura inexistente
		@Test
		public void eliminarFacturaInexistente(){
			GestorContabilidad gestorPruebas = new GestorContabilidad();
			gestorPruebas.eliminarFactura("789");
			fail("Se esperaba nullponterException");
		}
		//eliminar una factura existente
		@Test
		public void eliminarFacturaExistente(){
			GestorContabilidad gestorPruebas = new GestorContabilidad();
			gestorPruebas.getListaFacturas().clear();
			gestorPruebas.getListaFacturas().add(facturaPrueba);
			gestorPruebas.eliminarFactura("4");
			boolean x = gestorPruebas.getListaFacturas().contains(facturaPrueba);
			assertFalse(x);
		}
}
